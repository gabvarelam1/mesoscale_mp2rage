function [ res, tflag ] = waveSENSE( in, params, rcv, psfs,  tflag )


if strcmp(tflag,'transp');
    
    im = reshape(in, [params.psf_len, 1,1, params.num_chan]);
	 
    Im = repmat(im, [1,params.Ry,params.Rz, 1]);
    
    Temp = ifftc(Im .* conj(psfs), 1) * sqrt(params.psf_len);
    temp = Temp(end/2+1-params.img_len/2:end/2+params.img_len/2,:,:,:,:) .* conj(rcv);
    
    Res = sum(temp, 4);
    res = Res(:) ;

else

    im = reshape(in, [params.img_len, params.Ry, params.Rz, 1, size(psfs,5)]);

    img = repmat(im, [1,1,1, params.num_chan]) .* rcv;

    Img = padarray(img, [params.pad_size,0,0,0]);
    
    Img_conv = fftc(Img, 1) .* psfs / sqrt(params.psf_len);
    
    Res = squeeze(sum(sum(Img_conv, 2),3));
    res = Res(:);
end

end

