function sliceGroup = sliceGroupReconstruction(TIfile)

    load(TIfile);
    lsqr_iter = 300;
    lsqr_tol = 1e-4;
    
    sliceGroup = zeros([params.img_size(1:2),params.Rz],'single');
    
    % data is already sorted by slice groups
    for cey = params.cey_pos 

		% now collect data based on collapsing PE lines    
        cey_ind = cey : params.y_skip : cey + (params.Ry-1) *  params.y_skip;
	    psfs = repmat(psf_use_zi(:,cey_ind,:), [1,1,1,params.num_chan]);
	    rcv =reshape(receive_use_zi(:,cey_ind,:,:),[params.img_len, params.Ry, params.Rz, params.num_chan]);
        rhs = reshape(squeeze(wave_deblur_use_zi(:,cey,:,:)), [params.psf_len, 1,1, params.num_chan]);
       
        % apply SENSE for each Ry x Rz set of lines
        [res, flag, relres, iter] = lsqr(@waveSENSE,rhs(:), lsqr_tol, lsqr_iter, [], [], [], params,rcv, psfs);    
        sliceGroup(:,cey_ind,:) = reshape(res, [params.img_len, params.Ry, params.Rz]);
               
    end

end