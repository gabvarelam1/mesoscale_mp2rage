# mesoscale_MP2RAGE
Presented at ISMRM 2023 in Toronto, Canada. 

"Rapid Mesoscale MP2RAGE Imaging at Ultra High Field with Controlled Aliasing"
Gabriel Varela-Mattatall, Wei-Ching Lo, Omer Oran, 
Jonathan Polimeni, Azma Mareyam, Borjan Gagoski ,
Ravi S. Menon, Berkin Bilgic

# session
Program Number: #0539

Session Name: Pulse Sequences & Encoding Methods

Session Day: Tuesday, 06 June 2023

Session Time: 13:30 (Toronto time)

Presentation Time: 14:50 (Toronto time)

## log
2023/05/18 Creation of Project

2023/05/18 Cartesian 550um R9 (3x3)

2023/05/18 Codes to run SENSE recon in a slice Group

## Getting started
The data is sooooo big that I can upload only a slice group. Next update will be wave-caipi R9 (3x3) once I have eveything working. Nonetheless, the core algorithm does not change at all.

## References
Data and Codes are based on the following papers. I kindly appreciate to reference the following papers:
1. Wave-CAIPI - "Wave-CAIPI for Highly Accelerated 3D Imaging" MRM, 2015.

2. autoPSF - "Autocalibrated Wave-CAIPI Reconstruction; Joint Optimization of k-Space Trajectory and Parallel Imaging Reconstruction" MRM, 2017.

3. Wave-MPRAGE - "Wave-CAIPI for Highly Accelerated MP-RAGE Imaging" MRM, 2018.

4. 64-channel head-coil - "A 64-Channel 7T array coil for accelerated brain MRI". ISMRM, 2020.

5. Automatic Lambda for denoising/CS - "Automatic determination of the regularization weighting for wavelet-based compressed sensing MRI reconstructions", MRM, 2021.

