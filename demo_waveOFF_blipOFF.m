%% DEMO
clear all; close all; clc;
addpath utils/
addpath data/
%% Preamble
%%% this code runs on each inversion time to later compute the uniform
%%% image. The data corresponds to 550um-iso MP2RAGE. Obviously, the data
%%% is sooo big that I took a slice group. Because the data comes from R9
%%% (3x3) accelerated acquisition, the slice group contains 3 slices.

% MOST IMPORTANT
% WAVE ENCODING OFF -> corkscrew's psf is just a matrix with ones.
% CAIPIRINHA OFF -> there is no spatial shift within the FOV
% RECON MODE: SENSE -> grabs exactly the locations where the signal
% collapses, and it applies least-squares optimization to each specific
% set.

% SEQUENCE PARAMETERS
% TIs           900/2500 ms
% TR            5000 ms
% TE            3.43 ms
% Matrix Size*  432x432x336
% FOV           240x240x180mm

% *NOTE: data is from slice group 57 
%        that means slices [57,169,281]
%        or [pos : FOVz/Rz : end]
%% built-in functions

% visualization of slice group
showslice = @(im_,num_) (im_(:,:,num_));
showGroup = @(setg) [showslice(setg,1),showslice(setg,2),showslice(setg,3)];
m = @(setg) abs(showGroup(setg));   % show abs
u = @(setUNI) showGroup(setUNI);    % show real
vec = @(a)a(:);
maxv = @(setg) max(vec(m(setg)));

% to compute uniform image
UNI = @(TI1,TI2) real( (conj(TI1) .* TI2) ./ ( abs(TI1).^2 + abs(TI2).^2 ) );

%% T1
sgT1 = sliceGroupReconstruction('TI1_550um_blipOFF_waveOFF');

mxt1 = maxv(sgT1);
figure, imshow([m(sgT1)],[0,.3*mxt1])
%% T2 
sgT2 = sliceGroupReconstruction('TI2_550um_blipOFF_waveOFF');

mxt2 = maxv(sgT2);
figure, imshow([m(sgT2)],[0,.6*mxt2])
%% UNI image

uni550 = UNI(sgT1,sgT2);
figure, imshow(u(uni550),[-.4,.4]);